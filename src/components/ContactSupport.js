import React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import "../css/ContactSupport.css";
export class ContactSupport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="cta-wrapper">
        <Grid container spacing={8}>
          <Grid item xs={8}>
            <h1 className="cta-title">Can't find what you're looking for?</h1>
            <p className="cta-description">
              Get in touch with our team for specialized, in-depth assistance.
            </p>
          </Grid>
          <Grid item xs={4}>
            <Link to={"/"}>
              <p className="cta-button">
                Contact support
                <i className="fas fa-angle-right" />
              </p>
            </Link>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    posts: state.posts.posts,
    page: state.posts.ads,
    categories: state.categories,
    users: state.users,
  };
};

export default connect(mapStateToProps)(ContactSupport);
