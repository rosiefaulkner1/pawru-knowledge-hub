import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import moment from "moment";
import "../css/RecommendedArticles.css";

let posts = [];

export class RecommendedArticles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      postsList: posts,
    };
  }

  render() {
    return (
      <>
        <ul className="articles-wrapper">
        {posts.slice(0, 3).map((post) => (
            <div key={post._id}>
              <li className="each-article-wrapper">
                <h2 className="article-title">
                  <Link to={"/" + post.slug}>{post.title}</Link>
                </h2>
                <p className="article-date">
                  {moment(post.created).format("MMM Do YY")}
                </p>
                  <p className="article-excerpt">
                    {post.content
                      .replace(/<p>&quot;/g, "")
                      .replace(/<p>/g, "")
                      .replace(/<[^>]*>?/gm, "")
                      .slice(0, 100) + "..."}
                  </p>
              </li>
            </div>
          ))}
        </ul>
      </>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  posts = state.posts.posts;
  return {
    posts: state.posts.posts,
    page: state.posts.ads,
    categories: state.categories,
    users: state.users,
  };
};

export default connect(mapStateToProps)(RecommendedArticles);
