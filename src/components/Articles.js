import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export function Articles(props) {
  props.postKnowledgeBase &&
    props.postKnowledgeBase.map((item, index) => {
      return (
        <div key={index} className="main-knowledgebase-wrap">
          {item && (
            <img
              className="knowledgebase-image"
              src={item.metadata.image.url}
              alt={item.slug}
            />
          )}
          <div className="knowledgebase-text-wrap">
             to={"/" + item.slug}>
              {" "}<Link
              <h2>{item.title}</h2>
            </Link>
            {item.length < 300 ? (
              <p dangerouslySetInnerHTML={{ __html: item.content }} />
            ) : (
              <p
                dangerouslySetInnerHTML={{
                  __html: item.content.substring(0, 300),
                }}
              />
            )}
          </div>
        </div>
      );
    });
}
const mapStateToProps=(state, ownProps) => {
    if(ownProps.match) {
        var slug = ownProps.match.params.category_slug;
    } else{
        var main_slug = 'film'
    }
    
    return{
        postCategory:state.posts.posts.filter(cat => {
            if(slug) return cat.metadata.category.slug ===  slug 
            else return cat.metadata.category.slug ===  main_slug       
        })
    }
}

export default connect(mapStateToProps)(Articles);
