import React from "react";
import { connect } from "react-redux";
import "../css/SinglePost.css";
import moment from "moment";
import BreadCrumbs from "./BreadCrumbs";
import RecommendedArticles from "./RecommendedArticles";
import Rating from "./Rating";
import Grid from "@material-ui/core/Grid";

export function SinglePost(props) {
  return (
    <div>
      {props.post && (
        <div className="breadcrumbs-wrap">
        <BreadCrumbs
          category={props.post.metadata.category}
          post={props.post}
        />
        </div>
      )}
      <Grid container spacing={8}>
        <Grid item xs={6}>
          <h4>Internal Only</h4>
          {props.post && <h1 className="post-title">{props.post.title}</h1>}
          {props.post && (
            <span className="date">
              Posted {moment(props.post.created_at).format("MMM Do YY")}
            </span>
          )}
        </Grid>
        <Grid item xs={6}>
          <div className="post-buttons">
            <input
              type="button"
              className="button print"
              onClick={() => {
                window.print();
              }}
              value="PRINT"
            />
            <input
              type="button"
              className="button email"
              onClick={() => {
                window.open(
                  `mailto:?subject=${props.post.title}&body=${
                    window.location.href
                  }`
                );
              }}
              value="EMAIL"
            />
          </div>
        </Grid>
      </Grid>
      <hr className="post-divider" />
      <Grid container spacing={8}>
        <Grid item xs={6}>
          {props.post && (
            <p
              className="text-page"
              dangerouslySetInnerHTML={{ __html: props.post.content }}
            />
          )}
        </Grid>
        <Grid item xs={6}>
          <div className="post-image">
            {props.post &&
              (props.post.metadata.image.url && (
                <img
                  className="figure"
                  src={props.post.metadata.image.url}
                  alt=""
                />
              ))}
          </div>
        </Grid>

        <Grid item xs={6}>
          {props.post &&
            (props.post.metadata.campaign_description && (
              <div>
                <hr className="content-divider" />
                <h2>Campaign Description:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.campaign_description,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.criteria && (
              <div>
                <h2>Criteria:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.criteria,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.personalization_requirements && (
              <div>
                <h2>Personalization Requirements:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.personalization_requirements,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.frequency && (
              <div>
                <h2>Frequecy:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.frequency,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.trigger && (
              <div>
                <h2>Trigger:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.trigger,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.data_source_dependency_mvp && (
              <div>
                <h2>Data Source Dependency MVP:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.data_source_dependency_mvp,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.decision_makers_for_launch_party && (
              <div>
                <h2>Decision Makers for Launch Party:</h2>
                <p
                  className="campaign-description"
                  dangerouslySetInnerHTML={{
                    __html:
                      props.post.metadata.decision_makers_for_launch_party,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.message_stakeholders && (
              <div>
                <h2>Message Stakeholders:</h2>
                <p
                  className="documentation"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.message_stakeholders,
                  }}
                />
              </div>
            ))}

          {props.post &&
            (props.post.metadata.documentation && (
              <div>
                <hr className="documentation-divider" />
                <h2>Documentation:</h2>
                <p
                  className="documentation"
                  dangerouslySetInnerHTML={{
                    __html: props.post.metadata.documentation,
                  }}
                />
              </div>
            ))}
        </Grid>
      </Grid>
      <br />
      <Rating post={props.post} />
      <RecommendedArticles />
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  let slug = ownProps.match.params.post_slug;
  return {
    post: state.posts.posts.find((item) => item.slug === slug),
  };
};
export default connect(mapStateToProps)(SinglePost);
