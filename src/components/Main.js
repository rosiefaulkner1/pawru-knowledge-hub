import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "../css/Main.css";
import "../css/Pagination.css";
import moment from "moment";
import BreadCrumbs from "../components/BreadCrumbs";
import ReactPaginate from "react-paginate";
import SearchResultsHeader from "../components/SearchResultsHeader";

let allPosts,
  posts,
  filteredList,
  cats = [];
let pageCount,
  offset = 0;
let perPage = 2;

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterList: cats,
      searchLists: posts,
      activeFilter: [],
      searchForTerm: "",
      offset: offset,
      perPage: perPage,
      currentPage: 0,
    };
    this.checkBoxes = [];
    this.setCheckBoxRef = (element) => {
      this.checkBoxes.push(element);
    };
    this.filtersPreCheck = false;
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  preCheckFilters = () => {
    if (this.filtersPreCheck) {
      return;
    }
    if (!this.checkBoxes || this.checkBoxes.length < 1) {
      return;
    }
    let UrlParams = new URLSearchParams(this.props.location.search);
    let selectedCategories = UrlParams.getAll("category");
    selectedCategories.map((categorySlug) => {
      this.checkBoxes.map((checkBox) => {
        if (
          checkBox.getAttribute("data-slug") === categorySlug &&
          !checkBox.checked
        ) {
          checkBox.checked = true;
          this.onFilterChange(categorySlug);
        }
      });
    });
    this.filtersPreCheck = true;
  };

  componentDidUpdate() {
    setTimeout(
      function() {
        this.preCheckFilters();
      }.bind(this),
      500
    );
  }

  onFilterChange(filter) {
    const { filterList, activeFilter } = this.state;
    if (activeFilter.includes(filter)) {
      const filterIndex = activeFilter.indexOf(filter);
      const newFilter = [...activeFilter];
      newFilter.splice(filterIndex, 1);
      this.setState(
        {
          activeFilter: newFilter,
        },
        () => {
          this.handlePageClick({ selected: 0 });
        }
      );
    } else {
      this.setState(
        {
          activeFilter: [...activeFilter, filter],
        },
        () => {
          this.handlePageClick({ selected: 0 });
        }
      );
    }
  }

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;
    this.setFilteredList();
    posts = filteredList.slice(offset, offset + this.state.perPage);
    this.setState({
      currentPage: selectedPage,
      offset: offset,
    });
  };

  setFilteredList = () => {
    const { filterList, activeFilter } = this.state;
    let categories =
      this.props.categories.categories.length > 0
        ? this.props.categories.categories
        : filterList;
    if (
      activeFilter.length === 0 ||
      activeFilter.length === categories.length
    ) {
      filteredList = allPosts;
    } else {
      filteredList = allPosts.filter((item) =>
        this.state.activeFilter.includes(item.metadata.category.slug)
      );
    }
    pageCount = Math.ceil(filteredList.length / perPage);
  };

  render() {
    return (
      <div>
        <div className="breadcrumbs-wrap">
          <BreadCrumbs />
          <SearchResultsHeader />
        </div>
        <h1 className="search-results">Search Results...</h1>
        <div className="num-of-results">
          <p>Found {filteredList.length} Results</p>
        </div>
        <hr className="divider" />
        <div className="filter-wrap wrapper">
          <form className="left-column">
            <div className="filters">
              <h3>Categories</h3>
              <ul>
                {cats.map((filter) => (
                  <React.Fragment>
                    <li>
                      <input
                        id={filter._id}
                        type="checkbox"
                        ref={this.setCheckBoxRef}
                        className="checkbox-filter"
                        data-slug={filter.slug}
                        checked={this.state.activeFilter.includes(filter.slug)}
                        onClick={() => this.onFilterChange(filter.slug)}
                      />
                      <label htmlFor={filter._id}>
                        {filter.title} ({filter.postCount})
                      </label>
                    </li>
                  </React.Fragment>
                ))}
              </ul>
            </div>
          </form>
          <ul className="right-column">
            {posts.map((item) => (
              <div key={item._id}>
                <li>
                  <h2 className="card-title">
                    <Link to={"/" + item.slug}>{item.title}</Link>
                  </h2>
                  <p className="card-date">
                    {moment(item.created).format("MMM Do YY")}
                  </p>
                  <div className="card-excerpt">
                    <p>
                      {item.content
                        .replace(/<p>&quot;/g, "")
                        .replace(/<p>/g, "")
                        .replace(/<[^>]*>?/gm, "")
                        .slice(0, 300) + "..."}
                    </p>
                  </div>
                  <div className="read-more">
                    <p>
                      <Link to={"/" + item.slug}>Read More</Link>
                    </p>
                  </div>
                </li>
              </div>
            ))}
          </ul>
          <div className="pagination" />
          <ReactPaginate
            previousLabel={"<"}
            nextLabel={">"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={2}
            onPageChange={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  allPosts = filteredList = state.posts.posts;
  posts = allPosts.slice(offset, offset + perPage);
  pageCount = Math.ceil(allPosts.length / perPage);
  cats = state.categories.categories.map((cat) => {
    cat.postCount = 0;
    allPosts.forEach((post) => {
      if (post.metadata.category.slug === cat.slug) {
        cat.postCount++;
      }
    });
    return cat;
  });
  return {
    posts: allPosts,
    page: state.posts.ads,
    categories: state.categories,
    users: state.users,
  };
};

export default connect(mapStateToProps)(Main);
