import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/Input.css';
 class Input extends Component {
  render() {
    return (
        <div>
        <input type="text"  placeholder="&#xF002; What are you looking for today?" className="search-input" value={this.props.input} onChange={this.props.handleInput}/>
        </div>
    )
  }
}
const mapStateToProps = (state) => {
    return{
        input: state.input
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        handleInput: (e) => {
            dispatch({type:'SEARCH_INPUT', input:e.target.value})
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Input);