import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import ContactSupport from "./ContactSupport";
import "../css/Main.css";
import "../css/Front.css";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
  },
}));

function Front(props) {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>
        <Grid container spacing={8}>
          {props.users.users &&
            props.users.users.map((user, index) => {
              return (
                <Grid item xs={6} key={index}>
                  <Link to={"/" + user.slug}>
                    <Paper className={classes.paper}>
                      {user.title}
                      <br />
                    </Paper>
                  </Link>
                </Grid>
              );
            })}
          {props.categories.categories &&
            props.categories.categories.map((category, index) => {
              return (
                <Grid item xs={4} key={index}>
                  <Link to={"/" + category.slug}>
                    <Paper className={classes.paper}>
                      {category.title}
                      <br />
                    </Paper>
                  </Link>
                </Grid>
              );
            })}
          <hr className="front-divider" />
          <Grid item xs={12}>
            <ContactSupport />
          </Grid>
        </Grid>
      </div>
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    users: state.users,
  };
};
export default connect(mapStateToProps)(Front);
