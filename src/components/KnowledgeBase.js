import React from "react";
import { connect } from "react-redux";
import { KbFilter } from "./KbFilter";
import { Articles } from "./Articles";

import "../css/KnowledgeBase.css";

function KnowledgeBase(props) {
  return (
    <div>
      <KbFilter />
      <Articles />
    </div>
  );
}
const mapStateToProps = (state, ownProps) => {
  if (ownProps.match) {
    var slug = ownProps.match.params.category_slug;
  } else {
    var main_slug = "film";
  }

  return {
    postKnowledgeBase: state.posts.posts.filter((cat) => {
      if (slug) return cat.metadata.category.slug === slug;
      else return cat.metadata.category.slug === main_slug;
    }),
  };
};
export default connect(mapStateToProps)(KnowledgeBase);
