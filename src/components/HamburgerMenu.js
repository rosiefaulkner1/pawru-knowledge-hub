import { slide as Menu } from "react-burger-menu";
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "../css/HamburgerMenu.css";

export function HamburgerMenu(props) {
  let categoriesList = props.cats.slice(0, 8).map((cat, key) => (
    <li key={key}>
      <Link to={"/category/" + cat.slug} key={key}>{cat.title}</Link>
    </li>
  ));
  return (
    <Menu>
      <div className="navcategories">
        <Link id="categories" className="menu-item" to="/">
          Categories <span className="navcategoriesicon">&#xF107;</span>
        </Link>
        <ul>
          {categoriesList}
          <li>All Categories &#xf105;</li>
        </ul>
      </div>
      <hr className="nav-divider" />
      <Link id="request-content" className="menu-item main-item" to="/about">
        Request Content
      </Link>
      <hr className="nav-divider" />
      <Link id="visit" className="menu-item main-item" to="/contact">
        Visit Pawru.com
      </Link>
      <div className="social-media">
        <ul>
            <li>&#xf082;</li>
            <li>&#xf099;</li>
            <li>&#xf08c;</li>
            <li>&#xf167;</li>
            <li><img src="/pawru-logo.svg" alt="logo" width="100px" /></li>
        </ul>
      </div>
    </Menu>
  );
}

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    users: state.users,
  };
};
export default connect(mapStateToProps)(HamburgerMenu);
