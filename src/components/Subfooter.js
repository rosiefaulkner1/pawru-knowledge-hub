import React from "react";
import { Link } from "react-router-dom";
import "../css/Subfooter.css";

export default function Subfooter() {
  return (
      <>
    <div className="subfooter">
      <div className="subfooter-left">
        <p>Follow us on social</p>
      </div>
      <div className="subfooter-right">
        <img src="/pawru-logo.svg" alt="logo" width="250px" />
      </div>
    </div>
    <div className="socials">
    <div className="subfooter-left">
        <img src="/socials.png" alt="socials" width="100%" />
      </div>
      <div className="subfooter-right">
      <p>Placeholder legal text TM | PetPro Connect Powered By Pawru</p>
      </div>
    </div>
    </>
  );
}
