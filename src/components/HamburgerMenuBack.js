import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "../css/HamburgerMenu.css";

class HamburgerMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
    };
  }
  render() {
    return (
      <nav role="navigation">
        <div id="menuToggle">
          <input type="checkbox" />

          <span />
          <span />
          <span />

          <ul id="menu">
            <select value="Categories" name="Categories">
              {this.props.categories.categories &&
                this.props.categories.categories.map((category, index) => {
                  return (
                    // <Link to={"/" + category.slug} key={index}>
                    <option value="A" key={index}>
                      {category.title}
                    </option>
                    // </Link>
                  );
                })}
            </select>

            <Link to={"/"}>
              <li>Request Content</li>
            </Link>
            <Link to={"/"}>
              <li>Visit Pawru.com</li>
            </Link>
          </ul>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
  };
};
export default connect(mapStateToProps)(HamburgerMenu);
