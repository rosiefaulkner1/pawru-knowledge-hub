import React from "react";
import $ from "jquery";
import "../css/ReactForm.css";

const reactFormContainer = document.querySelector('.react-form-container')

class ReactFormLabel extends React.Component {
 constructor(props) {
  super(props)
 }

 render() {
  return(
   <label htmlFor={this.props.htmlFor}>{this.props.title}</label>
  )
 }
}

export default class ReactForm extends React.Component {
 constructor(props) {
  super(props)

  this.state = {
   category: '',
   type: '',
   content: ''
  }

  this.handleChange = this.handleChange.bind(this)
  this.handleSubmit = this.handleSubmit.bind(this)
 }

 handleChange = (e) => {
  let newState = {}

  newState[e.target.name] = e.target.value

  this.setState(newState)
 }


 handleSubmit = (e, message) => {
  e.preventDefault()

  let formData = {
   formSender: this.state.category,
   formEmail: this.state.type,
   formSubject: this.state.content
  }

  if (formData.formSender.length < 1 || formData.formEmail.length < 1 || formData.formSubject.length < 1 || formData.formMessage.length < 1) {
   return false
  }

  $.ajax({
   url: '/posts',
   dataType: 'json',
   type: 'POST',
   data: formData,
   success: function(data) {
    if (window.confirm('Thank you for your message. Can I erase the form?')) {
      this.setState({
        category: '',
        type: '',
        content: ''
      })
    }
   },
   error: function(xhr, status, err) {
    console.error(status, err.toString())
    alert('There was some problem with sending your message.')
   }
  })

  this.setState({
   category: '',
   type: '',
   content: ''
  })
 }

 render() {
  return(
   <form className='react-form' onSubmit={this.handleSubmit}>

    <fieldset className='form-group'>
     <ReactFormLabel htmlFor='formName' title='Feature category:' />

     <input id='formName' className='form-input' name='category' type='text' required onChange={this.handleChange} value={this.state.category} />
    </fieldset>

    <fieldset className='form-group'>
     <ReactFormLabel htmlFor='formContent' title='Type of content:' />

     <input id='formContent' className='form-input' name='type' type='text' required onChange={this.handleChange} value={this.state.type} />
    </fieldset>

    <fieldset className='form-group'>
     <ReactFormLabel htmlFor='formMessage' title='Content request:' />

     <textarea id='formMessage' className='form-textarea' name='content' required onChange={this.handleChange} value={this.state.content} ></textarea>
    </fieldset>

    <div className='form-group'>
     <input id='formButton' className='btn' type='submit' value='Submit request &#xF105;' />
    </div>
   </form>
  )
 }
}