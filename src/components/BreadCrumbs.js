import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import {
  Breadcrumbs,
  Link as MaterialLink,
  Typography,
} from "@material-ui/core";
import "../css/BreadCrumbs.css";

const BlueTextTypography = withStyles({
  root: {
    color: "#00aed8",
  },
})(Typography);

class BreadCrumbs extends Component {
  render() {
    return (
      <div>
        <div className="breadcrumbs">
          <Breadcrumbs maxItems={8} aria-label="breadcrumb">
            <MaterialLink color="inherit" href="/">
              Home
            </MaterialLink>
            {!this.props.isHome && (
              <MaterialLink color="inherit" href="/posts">
                Knowledge Base
              </MaterialLink>
            )}
            {this.props.category && (
              <MaterialLink
                color="inherit"
                href={"/category/" + this.props.category.slug}
              >
                {this.props.category.title}
              </MaterialLink>
            )}
            {this.props.post && (
              <MaterialLink
                color="inherit"
                href={
                  "/category/" +
                  this.props.category.slug +
                  "/" +
                  this.props.post.slug
                }
              >
                {this.props.post.title}
              </MaterialLink>
            )}
            <BlueTextTypography color="textPrimary" />
          </Breadcrumbs>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    input: state.input,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleInput: (e) => {
      dispatch({ type: "SEARCH_INPUT", input: e.target.value });
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BreadCrumbs);
