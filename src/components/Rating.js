import React, { Component } from "react";
import { connect } from "react-redux";
import { upvotePost, downvotePost } from "../store/actions/PostsAction";
import Cookies from "js-cookie";
import "../css/Rating.css";

class Rating extends Component {
  constructor(props) {
    super(props);
    console.log('this props');
    console.log(this.props);
    this.post = null;
    this.state = { isActive: true };
    this.upVote = this.upVote.bind(this);
    this.downVote = this.downVote.bind(this);
    this.ratedPostsUpdate = this.ratedPostsUpdate.bind(this);
  }
  upVote(e) {
    if (!this.state.isActive) {
      return;
    }
    this.manageState();
    upvotePost(this.post.slug);
  }
  downVote(e) {
    if (!this.state.isActive) {
      return;
    }
    this.manageState();
    downvotePost(this.post.slug);
  }
  manageState() {
    let cookies = Cookies.getJSON("rated-posts") || [];
    if (!cookies.includes("rated-posts")) {
      cookies.push(this.post.slug);
      Cookies.set("rated-posts", cookies);
    }
    this.setState({ isActive: !this.state.isActive });
  }
  ratedPostsUpdate() {
    if (this.props.post) {
      this.post = this.props.post;
      let ratedPosts = Cookies.getJSON("rated-posts") || [];
      this.setState({ isActive: !ratedPosts.includes(this.post.slug) });
    }
  }
  getDerivedStateFromProps(){
    this.ratedPostsUpdate();
  }
  render() {
    return (
      <div className="rating-wrap">
        <hr className="rating-divide" />
        <h3 className="rating-headline">Was this article helpful?</h3>
        <img
          className={this.state.isActive ? null : "disabled"}
          src="./thumbs-up.png"
          alt="thumbs-up"
          onClick={this.upVote}
        />
        <img
          className={this.state.isActive ? null : "disabled"}
          src="./thumbs-down.png"
          alt="thumbs-down"
          onClick={this.downVote}
        />
        <hr className="rating-divide" />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rating: state.rating,
  };
};

export default connect(mapStateToProps)(Rating);
