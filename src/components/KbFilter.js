import React from "react";

export function KbFilter(props) {
    const categories = props.categories.categories;
    const users = props.users.users;
    let state = {
      filters: {

      }
    }
    const filterPosts = (e) => {
      if (!state.filters.hasOwnProperty(e.target.name)) {
        state.filters[e.target.name] = [];
      }
      if (e.target.checked) {
        state.filters[e.target.name].push(e.target.value);
      } else {
        let idx = state.filters[e.target.name].indexOf(e.target.value);
        state.filters[e.target.name].splice(idx, 1);
      }
    }
  return (
    <div className="filter-wrap">
      <form action="" method="post">
        <div>
          <ul>
            {categories.length > 0 && (
              <li>
                <h4>Category</h4>
                <ul>
                  {categories &&
                    categories.map((category) => {
                      return (
                        <li key={category._id}>
                          <label>
                            <input
                              type="checkbox"
                              name={category.title}
                              value={category._id}
                              onChange={filterPosts}
                            />
                            {category.title}
                          </label>
                        </li>
                      );
                    })}
                </ul>
              </li>
            )}

            {users.length > 0 && (
              <li>
                <h4>User</h4>
                <ul>
                  {users &&
                    users.map((user) => {
                      return (
                        <li key={user._id}>
                          <label>
                            <input
                              type="checkbox"
                              name={user.title}
                              value={user._id}
                              onChange={filterPosts} 
                            />
                            {user.title}
                          </label>
                        </li>
                      );
                    })}
                </ul>
              </li>
            )}
          </ul>
        </div>
      </form>
    </div>
  );
}

export default KbFilter;
