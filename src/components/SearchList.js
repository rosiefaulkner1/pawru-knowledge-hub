import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "../css/SearchList.css";
export function SearchList(props) {
  return (
    <div className="search-list">
      <div className="suggestions">
        {props.value &&
          props.lists.map((item, index) => {
            return (
              <div>
              <li>
                <Link key={index} to={"/" + item.slug}>
                  <div className="it">{item.title}</div>
                </Link>
              </li>
              </div>
            );
          })}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    lists: state.posts.newList,
    value: state.posts.value,
    state: state.posts.state,
  };
};
export default connect(mapStateToProps)(SearchList);
