
import { combineReducers } from 'redux'
import postReducer from './postReducer';
import userReducer from './userReducer';
import categoryReducer from './categoryReducer';


const rootReducer = combineReducers({
    posts: postReducer,
    users: userReducer,
    categories: categoryReducer
})

export default rootReducer;