const initState = {
  users: [],
};
const userReducer = (state = initState, action) => {
  switch (action.type) {
    case "FETCH_USERS":
      return {
        ...state,
        users: action.data.objects.filter((user) => {
          return user.type_slug === "users";
        }),
      };
    default:
      return state;
  }
};

export default userReducer;
