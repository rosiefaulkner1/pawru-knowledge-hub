const initState = {
    products: [],
  };
  const productReducer = (state = initState, action) => {
    switch (action.type) {
      case "FETCH_PRODUCTS":
        return {
          ...state,
          products: action.data.objects.filter((product) => {
            return product.type_slug === "products";
          }),
        };
      default:
        return state;
    }
  };
  
  export default productReducer;
  