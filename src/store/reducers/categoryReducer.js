const initState = {
  categories: [],
};
const categoryReducer = (state = initState, action) => {
  switch (action.type) {
    case "FETCH_CATEGORIES":
      return {
        ...state,
        categories: action.data.objects.filter((cat) => {
          return cat.type_slug === "categories";
        }),
      };
    default:
      return state;
  }
};

export default categoryReducer;
