export const FetchProductsApi = () => {
    return (dispatch) => {
      const Cosmic = require("cosmicjs");
      var api = new Cosmic();
      var bucket = api.bucket({
        slug:
          process.env.REACT_APP_COSMIC_BUCKET ||
          "99c390e0-cac9-11eb-8151-359f5d333140",
        read_key:
          process.env.REACT_APP_COSMIC_READ_KEY ||
          "FkF5uC3sfwPJ89fsr71dRWGaWbWrFYd2D1JGxuQOZ13TfWTUo9",
      });
      bucket
        .getObjects({
          query: {
            type: "products",
          },
          props: "slug,title,content",
        })
        .then((data) => {
          dispatch({ type: "FETCH_PRODUCTS", data: data });
        })
        .catch((err) => {
          console.log(err);
        });
    };
  };
  