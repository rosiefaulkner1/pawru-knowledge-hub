const Cosmic = require("cosmicjs");
var api = new Cosmic();
var bucket = api.bucket({
  slug: "99c390e0-cac9-11eb-8151-359f5d333140",
  read_key: "FkF5uC3sfwPJ89fsr71dRWGaWbWrFYd2D1JGxuQOZ13TfWTUo9",
  write_key: "P5bPX8hg0NLQD3cEkGIyH1zLqst1hudLPJpjQH4THB9FrASg5s",
});

export const FetchPostsApi = () => {
  return (dispatch) => {
    bucket
      .getObjects({
        // query: {
        //   "metadata.categories.categories.slug": "Procedure",
        //   "type": "posts"
        // },
        //props: "slug,title,content",
      })
      .then((data) => {
        dispatch({ type: "FETCH_POSTS", data: data });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const upvotePost = (slug) => {
  return () => {
    bucket
      .getObject({
        slug: slug,
      })
      .then((data) => {
        let currentMetaUpvoteIdx = data.object.metafields.findIndex(x => x.key === 'upvote');
        data.object.metafields[currentMetaUpvoteIdx].value ++;
        bucket
          .editObject({
            slug: slug,
            metafields: data.object.metafields,
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const downvotePost = (slug) => {
  return () => {
    bucket
      .getObject({
        slug: slug,
      })
      .then((data) => {
        let currentMetaDownvoteIdx = data.object.metafields.findIndex(x => x.key === 'downvote');
        data.object.metafields[currentMetaDownvoteIdx].value ++;
        bucket
          .editObject({
            slug: slug,
            metafields: data.object.metafields,
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

// FetchPostsApi();
