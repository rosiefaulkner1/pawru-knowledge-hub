import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Input from "../components/Input";
import SearchList from "../components/SearchList";
import { HamburgerMenu } from "../components/HamburgerMenu";
import "../css/Navbar.css";

let hamburgercategories = [];
class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: hamburgercategories,
    };
  }
  render() {
    return (
      <div className="wrap-to-border">
        <div className="navbar-wrap">
          <div className="left-side">
            <HamburgerMenu cats={this.props.categories.categories} />
          </div>
          <div className="right-side">
            <Link to="/" className="signin">
              &#xf02e; My Content
            </Link>
          </div>
          <div className="title-wrap">
            <div className="search-wrap">
              <Input />
              <SearchList />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  hamburgercategories = state.categories.categories.map((cat) => {
    return cat;
  });
  return {
    categories: state.categories,
    users: state.users,
  };
};
export default connect(mapStateToProps)(Navbar);
