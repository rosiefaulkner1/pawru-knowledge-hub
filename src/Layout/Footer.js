import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Subfooter from "../components/Subfooter";
import RecommendedArticles from "../components/RecommendedArticles";
import ReactForm from "../components/ReactForm";
import "../css/Footer.css";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: "grid",
    gridTemplateColumns: "repeat(12, 1fr)",
    gridGap: theme.spacing(3),
  },
}));

function Footer(props) {
  const classes = useStyles();
  return (
    <>
      <div className="footer">
        <Grid container spacing={6}>
          <Grid item xs={3} sm={3}>
            <div className="row rowOne">
              <h3>Categories</h3>
              <div className="footer-cat-wrap">
                <ul className="footer-cat-list">
                  {props.categories &&
                    props.categories.map((category, key) => {
                      return (
                        <Link key={key} to={"/category/" + category.slug}>
                          <li className="cat-footer-item">{category.title}</li>
                        </Link>
                      );
                    })}
                  <li className="cat-footer-item">
                    All Categories
                    <span className="footercategoriesicon">&#xF105;</span>
                  </li>
                </ul>
              </div>
            </div>
          </Grid>
          <Grid item xs={3} sm={3}>
            <div className="row rowTwo">
              <h3>Recently Posted</h3>
              <RecommendedArticles />
            </div>
          </Grid>
          <Grid item xs={3} sm={3}>
            <div className="row">
              <h3>Request Content</h3>
              <ReactForm />
            </div>
          </Grid>
        </Grid>
      </div>
      <Subfooter />
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    categories: state.posts.categories,
  };
};
export default connect(mapStateToProps)(Footer);
